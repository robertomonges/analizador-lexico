/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos;

import Estructuras.*;

/**
 *
 * @author monges
 */
// se utiliza un dfa para validar una cadena de entrada
public class ResultadoValidacionAFD extends ResultadoValidacion {
    
    /**
     * Camino producido
     */
    private Conjunto<Par<Estado, String>> camino;
    // la clase resultado validación es un objeto que tiene los atributos de:
    // un automata, una entrada, un camino y simbolos faltantes que no fueron procesados, en caso de ser vacío
    // entonces si se consumieron todos los simbolos y la cadena fue aceptada por el lenguaje

    public ResultadoValidacionAFD(DFA automata, String entrada, 
        Conjunto<Par<Estado, String>> camino, String entradaFaltante) {
        
        this.automata = automata;
        this.entrada = entrada;
        this.camino = camino;
        this.entradaFaltante = (entradaFaltante == null) ? "" : entradaFaltante;
    }
    
    /**
     * El camino de <code>Estado</code>s que
     * resulta de validar la cadena de entrada.
     * @return Un <code>Conjunto</code> de <code>Estado</code>s
     * alcanzados durante la validación.
     */
    public Conjunto<Par<Estado, String>> getCamino() {
        return camino;
    }
    
    /**
     * Determina si el resultado de la validación
     * es válido o no. Es decir, si la cadena de
     * entrada es aceptada o no por el <code>Automata</code>.
     * @return <code>true</code> si la cadena de entrada
     * es aceptada por el <code>Automata</code>, <code>false</code>
     * en caso contrario.
     */
    public boolean esValido() {
        if (!entradaFaltante.equals(""))
            return false;
        
        if (camino.obtenerUltimo().getPrimero().getEsFinal())
            return true;
        else
            return false;
    }

    public String getEntradaFaltante() {
        return entradaFaltante;
    }
}

