/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritmos;

import Estructuras.*;
import java.util.*;

/**
 *
 * @author monges
 */
// Esta clase implementa los algoritmos de validación DFA
// utilizando el algoritmo de simulación del dfa en el libro
// aho 2da edición en la pág. 151, algoritmo 3.28
public class Validacion {

    public static ResultadoValidacion validarAFD(DFA afd, String entrada) {
        // cola que simulará un buffer de memoria
        Queue<String> buffer = new LinkedList<String>();
        
        // cargamos los símbolos del lexema a la lista buffer, que simulará el buffer de memoria
        for (int i=0; i < entrada.length(); i++)
            buffer.add("" + entrada.charAt(i));
        
        // Secuencia de estados recorridos 
        Conjunto<Par<Estado, String>> camino = new Conjunto<Par<Estado, String>>();
        
        // Comenzamos por el estado inicial del AFD */
        Estado estadoActual = afd.getEstadoInicial();
        camino.agregar(new Par<Estado, String>(estadoActual, ""));
        
        /* Cada simbolo de entrada */
        String simbolo;
        
        /* Recorremos mientras hayan simbolos en la entrada */
        while ((simbolo = buffer.poll()) != null) {
            /* Nos movemos al siguiente estado */
            estadoActual = mover(estadoActual, simbolo);
            
            if (estadoActual == null) {
                /* Si no se alcanza ningún estado, terminamos */
                break;
            }
            else {
                /* Agregar el estado nuevo al camino */
                camino.agregar(new Par<Estado, String>(estadoActual, simbolo));
            }
        }
        
        /*
         * Debemos comprobar cuál fue la condición que
         * provocó la finalización del ciclo while y 
         * reaccionar de acuerdo a ello.
         */
        if (estadoActual == null) {
            /*
             * Llegamos a un estado desde el cual
             * no se pudo avanzar con el símbolo
             * actual.
             * Recuperamos todos los simbolos no
             * consumidos.
             */
            String entradaFaltante = simbolo;
            for (String s : buffer)
                entradaFaltante += s;
            
            /*
             * Retornamos el camino construido y la
             * subcadena que faltó consumir de la
             * entrada.
             */
            return new ResultadoValidacionAFD(afd, entrada, camino, entradaFaltante);
        }
        else {
            /* 
             * Hemos consumido toda la entrada, por lo
             * que retornamos el camino construido y
             * la cadena vacía como entrada faltante.
             */
            return new ResultadoValidacionAFD(afd, entrada, camino, "");
        }
    }

    // esta función lo que hace es iterar la lista de transiciones que tiene dicho estado
    // buscando cual de ellos es la que pertenece a simbolo
    // asi para retornar el estado de destino
    private static Estado mover(Estado origen, String simbolo) {
        for (Transicion t : origen.getTransiciones())
            if (t.getSimbolo().equals(simbolo))
                return t.getEstado();
        
        return null;
    }
}

