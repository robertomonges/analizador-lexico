/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisis;

/**
 *
 * @author monges
 */
public class Token {
    // Identificador del token
    private TokenExprReg ident;
    
    // Valor del token
    private String valor;
    
    // aqui se listan los tokens conocidos
    public Token(TokenExprReg token) throws Exception {
        switch (token) {
            case CERRADURA_KLEENE:
                ident = TokenExprReg.CERRADURA_KLEENE;
                valor = "*";
                break;
            case CERRADURA_POSITIVA:
                ident = TokenExprReg.CERRADURA_POSITIVA;
                valor = "+";
                break;
            case CONCATENACION:
                ident = TokenExprReg.CONCATENACION;
                valor = "#";
                break;
            case UNION:
                ident = TokenExprReg.UNION;
                valor = "|";
                break;
            case PAREN_IZQUIERDO:
                ident = TokenExprReg.PAREN_IZQUIERDO;
                valor = "(";
                break;
            case PAREN_DERECHO:
                ident = TokenExprReg.PAREN_DERECHO;
                valor = ")";
                break;
            case FINAL:
                ident = TokenExprReg.FINAL;
                valor = "";
                break;
            default:
                throw new Exception("Token inválido");
        }
    }
    
    /**
     * Constructor para simbolos del alfabeto y para símbolos desconocidos.
     * @param token El tipo de token que deseamos crear.
     * @param simbolo Símbolo del alfabeto o desconocido para el token.
     * @throws Exception En caso de que <code>token</code> sea un tipo inválido.
     */
    public Token(TokenExprReg token, String simbolo) throws Exception {
        switch (token) {
            case ALFABETO:
                ident = TokenExprReg.ALFABETO;
                valor = simbolo;
                break;
            case DESCONOCIDO:
                ident = TokenExprReg.DESCONOCIDO;
                valor = simbolo;
                break;
            default:
                throw new Exception("Token inválido");
        }
    }
    
    // retorna identificador del token o clase
    public TokenExprReg getIdentificador() {
        return ident;
    }
    
    // retorna valor del token o clase
    public String getValor() {
        return valor;
    }

    @Override
    public String toString() {
        return valor;
    }
    
    @Override
    public boolean equals(Object obj) { // compara un token.equals(otroToken)
        if (obj == null) { // si está vacío entonces false
            return false;
        }
        
        if (getClass() != obj.getClass()) { // si con clases diferentes, false
            return false;
        }
        
        final Token other = (Token) obj;
        if (this.ident != other.ident) { // si los identificadores son diferentes, false
            return false;
        }
        
        return true; // clase e identificador iguales
    }  
}
