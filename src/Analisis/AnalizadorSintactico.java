/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Analisis;

import Algoritmos.*;
import Estructuras.*;

/**
 *
 * @author monges
 */
// convierte una er a un dfa
public class AnalizadorSintactico {
    
    
    // El analizador léxico para este analizador
    private AnalizadorLexico analizadorLexico;
    
    // Variable para el token actual.
    private Token preanalisis;
    
    // Contador de tokens recibidos. Útil para
    private int contadorTokens;
    
    // Constructor de la clase.
    public AnalizadorSintactico(Alfabeto alfabeto, String exprReg) {
        analizadorLexico = new AnalizadorLexico(alfabeto, exprReg); // el analizador sintactico tiene un analizador lexico
        contadorTokens = 0;
    }
    
    // retorna un NFA
    public NFA analizar() throws Exception {
        preanalisis = obtenerToken();
        
        if (preanalisis.getIdentificador() == TokenExprReg.FINAL)
            error("Expresión regular vacía");
        
        NFA afn = ExprReg();
        afn.setAlfabeto(analizadorLexico.getAlfabeto());
        afn.setExprReg(analizadorLexico.getExpresionRegular());
        
        if (preanalisis.getIdentificador() != TokenExprReg.FINAL)
            error("Carácter de finalización inválido");
        
        return afn;
    }
    
    /**
     * Método que procesa las uniones de la expresión regular.
     * @throws java.lang.Exception Propaga la excepción de Concat() y R1().
     */
    private NFA ExprReg() throws Exception {

        NFA afn1 = Concat();
        NFA afn2 = R1();
        
        if (afn2 == null)
            return afn1;
        else
            return Thompson.union(afn1, afn2);
    }
    
    /**
     * Método que procesa las uniones en forma de lista.
     * @throws java.lang.Exception Propaga la excepción de ExprReg().
     */
    private NFA R1() throws Exception {
        if (preanalisis.getIdentificador() == TokenExprReg.UNION) {
            match(preanalisis);
            
            NFA afn1 = Concat();
            NFA afn2 = R1();

            if (afn2 == null)
                return afn1;
            else
                return Thompson.union(afn1, afn2);
        }
        else {
            // Derivar en vacío
            return null;
        }
    }

    /**
     * Método que procesa una concatenación en la expresión regular.
     * @throws java.lang.Exception Propaga la excepción de Grupo() y R2().
     */
    private NFA Concat() throws Exception {
        NFA afn1 = Grupo();
        NFA afn2 = R2();
        
        if (afn2 == null)
            return afn1;
        else
            return Thompson.concatenacion(afn1, afn2);
    }
    
    /**
     * Método que procesa una concatenación en forma de lista.
     * @throws java.lang.Exception Propaga la excepción de Concat().
     */
    private NFA R2() throws Exception {
        switch (preanalisis.getIdentificador()) {
            case PAREN_IZQUIERDO:
            case ALFABETO:
                NFA afn1 = Grupo();
                NFA afn2 = R2();

                if (afn2 == null)
                    return afn1;
                else
                    return Thompson.concatenacion(afn1, afn2);
            default:
                // Derivar en vacío
                return null;
        }
    }
    
    // operadores que son unitarios, osea que operan sobre un solo nfa
    private NFA Grupo() throws Exception {
        NFA afn = Elem();
        TokenExprReg operador = Oper();
        
        switch (operador) {
            case CERRADURA_KLEENE:
                return Thompson.cerraduraKleene(afn);
            case CERRADURA_POSITIVA:
                return Thompson.cerraduraPositiva(afn);
            default:
                return afn;
        }
    }
    
    /**
     * Método que procesa un operador en la expresión regular.
     * @throws java.lang.Exception Propaga la excepción de match().
     */
    private TokenExprReg Oper() throws Exception {
        TokenExprReg operador;
        
        switch (preanalisis.getIdentificador()) {
            case CERRADURA_KLEENE:
            case CERRADURA_POSITIVA:
                operador = preanalisis.getIdentificador();
                match(preanalisis);
                break;
            default:
                // Derivar en vacío
                operador = TokenExprReg.DESCONOCIDO;
        }
        
        return operador;
    }
  
    /**
     * Método que procesa un elemento unitario en la expresión
     * regular. Se intenta hacer match con el paréntesis de
     * apertura o con algún símbolo del alfabeto. En caso 
     * contrario, se produce un error.
     * @throws java.lang.Exception En caso de que no se encuentre un símbolo
     * del alfabeto ni un paréntesis de apertura (inicio de una nueva expresión
     * regular).
     */
    private NFA Elem() throws Exception {
        NFA afn = null;
        
        switch (preanalisis.getIdentificador()) {
            case PAREN_IZQUIERDO:
                
                match(new Token(TokenExprReg.PAREN_IZQUIERDO));
                afn = ExprReg();
                match(new Token(TokenExprReg.PAREN_DERECHO));
                break;
            case ALFABETO:
                
                afn = SimLen();
                break;
            default:
                error("Se espera paréntesis de apertura o símbolo de alfabeto. " +
                    "Se encontró \"" + preanalisis.getValor() + "\"");
        }
        
        return afn;
    }
    
    /**
     * Método que procesa un símbolo del alfabeto en la expresión regular.
     * @throws java.lang.Exception Si el caracter actual no es un símbolo del alfabeto.
     */
    private NFA SimLen() throws Exception {
        String simbolo = preanalisis.getValor();
        
        if (!analizadorLexico.getAlfabeto().contiene(simbolo)) {
            error("El símbolo \"" + simbolo + 
                "\" no pertenece al alfabeto definido.");
        }
        
        NFA afn = Thompson.basico(simbolo);
        match(preanalisis);
        return afn;
    }

    /**
     * Método que se encarga de corroborar que la
     * entrada es la correcta para consumir el siguiente
     * token.
     * @param entrada Token esperado, debe ser igual al token actual.
     * @throws java.lang.Exception En caso de que el token actual no
     * sea igual al esperado.
     */
    private void match(Token entrada) throws Exception {
        if (preanalisis.equals(entrada))
            preanalisis = obtenerToken(); // retorna nuevo simbolo o token para ser analizado
        else if (entrada.getIdentificador() == TokenExprReg.PAREN_DERECHO)
            error("Falta paréntesis de cierre");
        else
            error("Carácter inválido");
    }
    
    /**
     * Método que se encarga de lanzar excepciones
     * para los distintos casos de error posibles.
     * @param mensaje El mensaje de error.
     * @throws java.lang.Exception Siempre se lanza una excepción,
     * producto del error ocurrido.
     */
    private void error(String mensaje) throws Exception {
        String mensajeCompleto = "";
        
        mensajeCompleto += "Error de sintáxis\n";
        mensajeCompleto += "Carácter: " + preanalisis.getValor() + "\n";
        mensajeCompleto += "Posición: " + contadorTokens + "\n";
        mensajeCompleto += "Mensaje : " + mensaje;
        
        throw new Exception(mensajeCompleto);
    }
    
    /**
     * Método que obtiene el siguiente token y registra
     * la cantidad de tokens leídos.
     * @return El siguiente token del Analizador Léxico.
     * @throws java.lang.Exception
     */
    private Token obtenerToken() throws Exception {
        contadorTokens++;
        return analizadorLexico.sgteToken();
    }
    
}
