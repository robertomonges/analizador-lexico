/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentacion;

import Algoritmos.*;
import Analisis.*;
import Estructuras.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author monges
 */
// la arquitectura del analizador léxico está diseñado como lo explica el libro de aho, 2da edición,
// en la página 135, el primer caso, de tomar cada token y hacer un arreglo de ellos, probando 
// el lexema uno por uno, en caso de que no sea válido, entonces se prueba con el siguiente.
// es decir, todos contra todos, diagramas (en este caso DFA) vs Lexemas
public class Main {

    public static void main(String[] args) throws Exception {
        Scanner teclado = new Scanner(System.in); // scanner para leer datos por teclado
        Vector<String> er = new Vector();   // contiene la lista de las expresiones regulares
        Vector<String> alfa = new Vector(); // contiene la lista de los alfabetos
        Vector<String> lex = new Vector(); // contiene la lista de los lexemas a poner a prueba
        String entradaLexemas = null;

        String cadena;
        try {
            FileReader f = new FileReader("entrada.txt");
            BufferedReader b = new BufferedReader(f);
            entradaLexemas = b.readLine();
            while ((cadena = b.readLine()) != null) {
                String linea[] = cadena.split(",");
                er.add(linea[0]);
                alfa.add(linea[1]);
            }
            b.close();

            // convertimos los lexemas en lista de lexemas
            String[] auxLex = null;
            auxLex = entradaLexemas.split(" ");
            lex.addAll(Arrays.asList(auxLex));

            for (int i = 0; i < er.size(); i++) {// aqui iteramos todos los elementos de expresiones regulares 
                Alfabeto alfabeto;
                alfabeto = new Alfabeto(alfa.get(i));// aqui el indice de cada iteración para er también sirve para su alfabeto

                AnalizadorSintactico as = new AnalizadorSintactico(alfabeto, er.get(i));
                NFA afn = as.analizar();
                DFA afd = Subconjuntos.getAFD(afn);
                DFAMin afdMin = Minimizacion.getAFDminimo(afd);
                Main m = null;

                System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
                System.out.println("ER: " + er.get(i));
                System.out.printf("Tabla de Transición del NFA:\n%s", afn);
                System.out.printf("Tabla de Transición del DFA:\n%s", afd);
                System.out.printf("Tabla de Transición del DFAmin:\n%s", afdMin.getAfdPostMinimizacion());

                for (int j = 0; j < lex.size(); j++) { // realizamos una iteración de cada dexema para verificar en la expresion regular i
                    String entrada = lex.get(j);

                    ResultadoValidacion rv = Validacion.validarAFD(afdMin.getAfdPostIdentidades(), entrada);
                    System.out.println(rv.getEntradaFaltante());
                    if (rv.esValido()) {
                        System.out.println("El lexema " + entrada + " es aceptada y pertenece a la clase de ER: " + er.get(i));
                    } else {
                        System.out.println("Error, el lexema " + entrada + " no pertenece a la clase de ER: " + er.get(i));
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Error al leer archivo entrada.txt");
            System.out.println("El archivo en la primera línea debe contener lista de lexemas separadas por 1 espacio");
            System.out.println("En las demás líneas debe tener (expresion regular, alfabeto)");
        }
    }
}
