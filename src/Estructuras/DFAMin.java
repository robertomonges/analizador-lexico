/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

/**
 *
 * @author monges
 */
public class DFAMin {
    private DFA afdOriginal;
    
    /**
     * AFD resultante luego de aplicar la
     * eliminación de estados inalcanzables.
     */
    private DFA afdPostInalcanzables;
    
    /**
     * AFD resultante luego de aplicar el
     * algoritmo de minimización.
     */
    private DFA afdPostMinimizacion;
    
    /**
     * AFD resultante luego de aplicar la
     * eliminación de estados identidades
     * no finales.
     */
    private DFA afdPostIdentidades;
    
    /**
     * Construye un <code>AFDMin</code>.
     * @param afdOriginal El <code>AFD</code> a partir del cual fue construido este <code>AFDMin</code>.
     * @param afdPostInalcanzables El <code>AFD</code> resultante de la eliminación de estados inalcanzables.
     * @param afdPostMinimizacion El <code>AFD</code> resultante del proceso de minimización.
     * @param afdPostIdentidades El <code>AFD</code> resultante de la eliminación de estados identidades.
     */
    public DFAMin(DFA afdOriginal, DFA afdPostInalcanzables, DFA afdPostMinimizacion, DFA afdPostIdentidades) {
        this.afdOriginal          = afdOriginal;
        this.afdPostInalcanzables = afdPostInalcanzables;
        this.afdPostMinimizacion  = afdPostMinimizacion;
        this.afdPostIdentidades   = afdPostIdentidades;
    }
    
    /**
     * Obtiene el <code>AFD</code> a partir del cual fue construido este <code>AFDMin</code>.
     * @return El <code>AFD</code> a partir del cual fue construido este <code>AFDMin</code>.
     */
    public DFA getAfdOriginal() {
        return afdOriginal;
    }

    /**
     * Obtiene el <code>AFD</code> resultante de la eliminación de estados inalcanzables.
     * @return El <code>AFD</code> resultante de la eliminación de estados inalcanzables.
     */
    public DFA getAfdPostInalcanzables() {
        return afdPostInalcanzables;
    }

    /**
     * Obtiene el <code>AFD</code> resultante del proceso de minimización.
     * @return El <code>AFD</code> resultante del proceso de minimización.
     */
    public DFA getAfdPostMinimizacion() {
        return afdPostMinimizacion;
    }

    public DFA getAfdPostIdentidades() {// dfa resultante despues del algoritmo de minimización
        return afdPostIdentidades;
    }
    
    /**
     * Verifica si la eliminación de estados inalcanzables produjo algún
     * cambio sobre el <code>AFD</code> original.
     * @return <code>true</code> si la eliminación de estados inalcanzables
     * produjo algún cambio sobre el <code>AFD</code> original, <code>false</code>
     * en caso contrario.
     */
    public boolean inalcanzablesEliminados() {
        if (afdOriginal.toString().equals(afdPostInalcanzables.toString()))
            return false;
        else
            return true;
    }
    
    /**
     * Verifica si la eliminación de estados identidades produjo algún
     * cambio sobre el <code>AFD</code> resultante de la minimización.
     * @return <code>true</code> si la eliminación de estados identidades
     * produjo algún cambio sobre el <code>AFD</code> resultante de la 
     * minimización, <code>false</code> en caso contrario.
     */
    public boolean identidadesEliminados() {
        if (afdPostMinimizacion.toString().equals(afdPostIdentidades.toString()))
            return false;
        else
            return true;
    }  
}

