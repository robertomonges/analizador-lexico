/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estructuras;

import Analisis.Alfabeto;

/**
 *
 * @author monges
 */
/**
 * Clase que representa la abstracción para un Autómata Finito
 * No determinístico (AFN). Un AFN es contruído a partir de una 
 * expresión regular a través de las construcciones de Thompson.
 */
public class NFA extends Automata {
        
    /**
     * Constructor por defecto.
     */
    public NFA() {
       super();
    }
    
    /**
     * Construye un <code>AFN</code> con un determinado <code>Alfabeto</code>
     * y una determinada expresión regular.
     * @param alfabeto El <code>Alfabeto</code> de este <code>AFN</code>.
     * @param exprReg La expresión regular para este <code>AFN</code>.
     */
    public NFA(Alfabeto alfabeto, String exprReg) {
        super(alfabeto, exprReg);
    }
    
    /**
     * Retorna la tabla de transición de estados.
     * @return La tabla de transición de estados.
     */
    public TablaTransicion getTablaTransicion() {
        int cantFil = getEstados().cantidad();
        int cantCol = getAlfabeto().getCantidad() + 2;
        
        return cargarTablaTransicion(cantFil, cantCol, 0);
    }
}

